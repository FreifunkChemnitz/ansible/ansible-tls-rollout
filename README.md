# ansible-role-tls-rollout

## usage example
```
---
- hosts: localhost
  tasks:
    - name: create cert on local machine
      include_role:
        name: tls-rollout
        apply:
          become: yes
          vars:
            domain: "{{ item }}"
            cert_path_src: "/path/to/certs"
      with_items:
        - example.com